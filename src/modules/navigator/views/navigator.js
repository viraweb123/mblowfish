
import templateUrl from './navigator.html';

export default {
	title: 'Navigator',
	description: 'Navigate all path and routs of the pandel',
	controller: 'MbNavigatorCtrl',
	controllerAs: 'ctrl',
	templateUrl: templateUrl,
	groups: ['Utilities']
}


